### Test an HTTP API with
# Behat & Behapi


### Pierre Tachoire

backend dev [@wisembly](http://wisembly.com/)

[Remote worker](http://krichprollsch.gitlab.io/teletravail)

[@krichprollsch](https://twitter.com/krichprollsch)


### DISCLAIMER

> Cette présentation n'est pas une présentation sur le cyclimse

<small>Merci de votre compréhension</small>

---

### Behat

> A php framework for autotesting your business expectations.

[behat.org](http://behat.org/)



```
  Scenario: Buying a single product under £10
    Given there is a "Sith Lord Lightsaber", which costs £5
    When I add the "Sith Lord Lightsaber" to the basket
    Then I should have 1 product in the basket
    And the overall basket price should be £9
```

[gherkin syntax](http://behat.org/en/latest/user_guide/gherkin.html)


```php
/**
 * @Given there is a(n) :arg1, which costs £:arg2
 */
public function thereIsAWhichCostsPs($arg1, $arg2)
{
    throw new PendingException();
}
```

---

> Nice, but how to test a real HTTP API?

---

### Behapi

> Behat extension to help write describe features related to HTTP APIs.

[github.com/Taluu/Behapi](https://github.com/Taluu/Behapi)


### DISCLAIMER

```
    "require": {
        "php": "^7.1",
```


### A GET endpoint?

```
Scenario: retrieve a blog post
  When I create a "GET" request to "/posts/say-hello-to-behapi"
  And I send the request
  Then the status code should be 200
  And the content-type should be equal to "application/json"
  And in the json, "data.id" should be equal to "1234"
  And in the json, "data.title" should be equal to "Behapi FTW"
```


### Do you prefer POST something?

```
Scenario: Post a new blog post
  When I create a "POST" request to "/posts"
  And I set the following body:
  """
  {
      "title": "Say hello to Behapi",
      "content": "Test your HTTP API easily with Behapi"
  }
  """
  And I send the request
  Then the status code should be 200
  And in the json, "data.id" should not be null
```


![so easy](images/easy.gif)

---

### Hunder the hood


### Contexts

* [http](https://github.com/Taluu/Behapi/blob/master/src/Context/Http.php)
* [json](https://github.com/Taluu/Behapi/blob/master/src/Context/AbstractJson.php)


### HTTPlug

[httplug](http://httplug.io/)

---

### DEMO

![demo](images/demo.gif)


[krichprollsch/behapi-demo](https://gitlab.com/krichprollsch/behapi-demo)

---

### Some tricks


### debug

When some tests failed, you can use the `--behapi-debug` option with behat in order
to have some nice logs of your requests.

```
And in the json, "foo.bar" should be equal to "baz"        # Behapi\Context\Json::theJsonPathShouldBeEqualTo()
 Expected a value equal to "baz". Got: "bar" (InvalidArgumentException)

 | Request : POST http://127.0.0.1:2080/echo
 | Request Content-Type : application/json

 | Response status : 200 OK
 | Response Content-Type : application/json

 {"foo":{"bar":"bar"}}
```


### send a query parameter

```
When I set the value "2" to the parameter "page"
```

`/posts?page=2`


### send a header

```
When I set the value "application/json" to the header "Content-type"
```


### check a header

```
Then the response header "Content-type" should be "application/json"
And the response should have a header "Content-type"
```


### check pathes

```
Then "data.title" should be accessible in the latest json response
And "data.id" should not exist in the latest json response
And in the json, "data.foo" should be equal to "bar"
And in the json, "data.baz" should be null
And in the json, "data.id" should not be null
And in the json, "data.title" should contain "Behapi"
```


### check collections

```
Then in the json, "data.posts" should have "5" elements
Then in the json, "data.posts" should have at least "4" elements
Then in the json, "data.posts" should have at most "6" elements
Then in the json, "data.posts[0].id" should be equal to "123"
```


More to discover: [Context/AbstractJson](https://github.com/Taluu/Behapi/blob/master/src/Context/AbstractJson.php)

![](images/make-it-rain.gif)

---

### How to extend?

![](images/tada.gif)


### Create a new step

```
Scenario: Create a blog post
  Given I am an authenticated user
  When I create a "POST" request to "/posts"
```


### Run Behat

```
$ ./vendor/bin/behat
 >> main suite has undefined steps. Please choose the context to generate snippets:

--- Behapi\Context\Http a des étapes manquantes. Définissez-les avec les modèles suivants :

    /**
     * @Given I am an authenticated user
     */
    public function iAmAnAuthenticatedUser()
    {
        throw new PendingException();
    }
```


### Extend the Behapi\Http Context class

```
use Behapi\Context\Http as BaseHttp;
use Behapi\Extension\Context\ApiTrait;

class Http extend BaseHttp
{
    use ApiTrait;
```

It's easier to don't rewrite the `__construct`.


### Use your own Http Context class

into behat.yml

```
contexts:
  - Http:
    - "@http.client"
    - "@http.stream_factory"
    - "@http.message_factory"
    - "@http.history"
```


### Implement your step

use HTTPlug to make the requests

```
private $token;

/**
 * @Given I am an authenticated user
 */
public function iAmAnAuthenticatedUser()
{
    $request = $this->messageFactory->createRequest('POST', '/auth/session');
    $response = $this->client->sendRequest($request);

    // TODO: deal with response errors

    $this->token = json_decode($response->getBody(), true);
}
```

---

### Thank you

![](images/thank-you.gif)
