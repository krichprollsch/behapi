# Test an API HTTP with Behat & Behapi

Feedback from the use of [Behat](http://behat.org/) and
[Behapi](https://github.com/Taluu/Behapi) at [Wisembly](https://wisembly.com/).

Talk givin during the [Clermont'ech.php#1](http://clermontech.org/groups/clermontech-php-1.html)
organised by [Clermont'ech](http://clermontech.org/)

See the [slides](http://krichprollsch.gitlab.io/behapi/index.html)
